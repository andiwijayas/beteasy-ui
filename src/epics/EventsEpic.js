import * as Constants from '../constants';
import { switchMap, catchError, takeUntil, map } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { getEvents } from '../services/EventsService';
import { eventsSuccess, eventsFailed } from '../actions';
import {  timer } from 'rxjs';

const EventsEpic = action$ => {
    const stopPolling$ = action$.pipe(
        ofType(Constants.STOP_POLLING_EVENT)
    )

    return action$.pipe(
        ofType(Constants.START_POLLING_EVENT),
        switchMap(($action) => {
            return timer(0, 1000).pipe(
                takeUntil(stopPolling$),
                switchMap(async ($action) => await getEvents('https://s3-ap-southeast-2.amazonaws.com/bet-easy-code-challenge/next-to-jump')),
                map(response => eventsSuccess(response)),        
                catchError(err => Promise.resolve(eventsFailed(err.message)))
            )
        })
    ) 
}
export default EventsEpic