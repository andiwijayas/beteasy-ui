import EventsEpic from './EventsEpic';
import * as Constants from '../constants'
import { of } from 'rxjs'
import { pollEvents } from '../actions'

it('should emit a EVENT_SUCCESS action with the events info as a payload', () => {
    const mockedObject = {
        mocked: true
    }    
    jest.mock('../services/EventsService', () => () => mockedObject)
    const action = pollEvents('https://s3-ap-southeast-2.amazonaws.com/bet-easy-code-challenge/next-to-jump')
    const action$ = of(action)
    const epic$ = EventsEpic(action$)

    epic$.subscribe((action) =>{
        expect(action.type).toBe(Constants.EVENT_SUCCESS)
        expect(action.events).toBe(mockedObject)
    })
});
