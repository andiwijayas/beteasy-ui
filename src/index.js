import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App/App';
import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import reducers from './reducers';
import { Provider } from 'react-redux';
import EventsEpic from './epics/EventsEpic';
import { pollEvents } from './actions';

const epicMiddleware = createEpicMiddleware()
const store = createStore(reducers, applyMiddleware(epicMiddleware))
epicMiddleware.run(EventsEpic)

store.dispatch(pollEvents('https://s3-ap-southeast-2.amazonaws.com/bet-easy-code-challenge/next-to-jump'))

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root')
);

