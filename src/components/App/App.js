import React from 'react';
import './App.css';
import NavBar from '../NavBar/NavBar';
import Widget from '../Widget/Widget';

const App = () => {
  return (
    <div className="App">
        <div className="left-gap"></div>
        <div>
            <NavBar />        
            <div className="content">
                <Widget />>
            </div>
        </div>
        <div className="right-gap"></div>
    </div>
  );
}

export default App;
