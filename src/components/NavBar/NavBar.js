import React from 'react';
import { BetEasyLogo, RacingLogo } from '../Svg';
import './NavBar.css';

function NavBar() {
    return (
        <div className="navbar">
            <div className="logo">
                <a className="link" href="http://www.beteasy.com.au">
                    <BetEasyLogo className = "icon-betEasy" />
                </a>
            </div>
            <div className="navbar-item">
                <a className="navbar-link" href="http://www.beteasy.com.au">
                    <RacingLogo className = "navbar-icon"></RacingLogo>
                    <div className="navbar-text">Racing</div>
                </a>                            
            </div>
        </div>
    );
}

export default NavBar;