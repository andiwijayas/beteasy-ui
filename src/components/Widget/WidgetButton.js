import React from 'react'
import classnames from 'classnames';

const WidgetButton = ({isActive, onClick, children}) => {
    return (
        <div className={classnames('button', {'selected' : isActive})} onClick={onClick} >
            {children}
        </div>        
    )
}

export default WidgetButton