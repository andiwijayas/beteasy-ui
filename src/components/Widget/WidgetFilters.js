import React from 'react';
import { ThoroughBredIcon, GreyhoundsIcon, TrotsIcon } from '../Svg';
import * as Constants from '../../constants';
import { useSelector, useDispatch } from 'react-redux';
import WidgetButton from './WidgetButton';
import { selectFilter } from '../../actions';

const WidgetFilters = () => {    
    const dispatch = useDispatch()
    const filter = useSelector(state => state.widget.selectedFilter)
    const setSelectedFilter = (newFilter) => {
        dispatch(selectFilter(newFilter))
    }
    return (
        <div className="filter">
            <WidgetButton 
                isActive={filter === Constants.ALL}
                onClick={() => setSelectedFilter(Constants.ALL)}
            >
                All
            </WidgetButton>
            <WidgetButton
                isActive={filter === Constants.THOROUGHBRED}
                onClick={() => setSelectedFilter(Constants.THOROUGHBRED)}            
            >
                <ThoroughBredIcon className = "button-icon" />
            </WidgetButton>
            <WidgetButton
                isActive={filter === Constants.GREYHOUNDS}
                onClick={() => setSelectedFilter(Constants.GREYHOUNDS)}            
            >
                <GreyhoundsIcon className = "button-icon" />
            </WidgetButton>
            <WidgetButton
                isActive={filter === Constants.TROTS}
                onClick={() => setSelectedFilter(Constants.TROTS)}                        
            >
                <TrotsIcon className = "button-icon" />
            </WidgetButton>                                
        </div>                

    )
}

export default WidgetFilters;