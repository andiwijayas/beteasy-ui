import React from 'react'
import { useSelector } from 'react-redux'
import * as Constants from '../../constants'
import { ThoroughBredIcon, GreyhoundsIcon, TrotsIcon } from '../Svg'
import moment from 'moment'

const EventLogo = ({logoType, className}) => {
    switch (logoType) {
        case Constants.THOROUGHBRED : return (<ThoroughBredIcon className={className}/>)
        case Constants.TROTS : return (<TrotsIcon className={className}/>)
        case Constants.GREYHOUNDS : return (<GreyhoundsIcon className={className} />)  
        default: return null              
    }
} 

const Event = ({event, index}) => {    
    return (
        <div className="event">
            <div className="event-logo">
                <EventLogo logoType={event.EventTypeDesc} className="event-item" />
            </div>
            <div className="event-location">
                {event.Venue.Venue}
            </div>
            <div className="event-race">
                {event.EventName}
            </div>
            <div className="event-countdown">
                {moment(moment(event.AdvertisedStartTime).diff(moment())).format("m[m] s[s]")}
            </div>
        </div>

    );
}

const WidgetEvents = () => {
    const { events, selectedFilter, error } = useSelector(state => state.widget);
    
    const eventView = events && events.length 
                        ? events
                            .filter((e) => selectedFilter === Constants.ALL 
                                            || selectedFilter === e.EventTypeDesc)
                            .map ((e, index) => (<Event key={index} event={e} />)) 
                        : <div>No event</div>

    const errorView = <span>Cannot retrieve data, error: {error}</span> 
    
    return (        
        <div className="events">
        { 
            (error) 
                ? errorView 
                : eventView 
        }         
        </div>
    )
}

export default WidgetEvents