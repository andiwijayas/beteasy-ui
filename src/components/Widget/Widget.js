import React from 'react';
import WidgetFilters from './WidgetFilters';
import WidgetEvents from './WidgetEvents';
import './Widget.css';

const Widget = () => {    
    return (
        <div className="widget">
            <div className="widget-header">Next to Jump</div>
            <WidgetFilters /> 
            <WidgetEvents />
        </div>
    )
}

export default Widget