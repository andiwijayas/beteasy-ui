import * as Constants from '../constants';

export const fetchEvents = (url) => {return {type: Constants.EVENT_FETCH, url: url}}
export const eventsSuccess = (events) => {return {type: Constants.EVENT_SUCCESS, events: events}}
export const eventsFailed = (message) => {return {type: Constants.EVENT_FAIL, message: message}}
export const selectFilter = (filter) => {return {type: Constants.SETSELECTEDFILTER, filter: filter}}
export const pollEvents = (url) => {return {type: Constants.START_POLLING_EVENT, url: url}}


