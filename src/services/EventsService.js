import fetch from 'node-fetch';

export const getEvents = async (url) => {
    return await fetch(url).then(r => r.json()).then(r=>r.result);
}