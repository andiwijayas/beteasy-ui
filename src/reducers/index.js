import {combineReducers} from 'redux';
import * as Constants from '../constants'

const initialState = {
    selectedFilter: Constants.ALL,
    events: [],
    error: undefined
}

const widgetReducer = (state=initialState, action) => {
    switch (action.type) {
        case Constants.SETSELECTEDFILTER: 
        return Object.assign({}, state, {selectedFilter: action.filter})
        case Constants.EVENT_SUCCESS:
            return Object.assign({}, state, {events: action.events, error: undefined})
        case Constants.EVENT_FAIL:
            return Object.assign({}, state, {error: action.message, events:[]})
        default:
            return state
    }
}

export default combineReducers ({
    widget: widgetReducer 
})